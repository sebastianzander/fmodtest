﻿using FMODUnity;
using UnityEngine;
using static FloatExtensions;

public class FmodParameter : MonoBehaviour
{
    [SerializeField]
    private StudioEventEmitter eventEmitter = null;

    [SerializeField]
    private string parameterName = "";

    [SerializeField]
    [Tooltip("Whether to remap the input range [0; 1] to another output range")]
    private bool remapRange = false;

    [SerializeField]
    [Tooltip("The output range that the FMOD Studio parameter expects")]
    private FloatRange outputRange = new FloatRange() { from = 0f, to = 100f };

    private float value = 0f;
    public float Value
    {
        get => value;
        set {
            this.value = Mathf.Clamp(value, 0f, 1f);
            UpdateParameter();
        }
    }

    private void OnValidate()
    {
        UpdateParameter();
    }

    private void UpdateParameter()
    {
        eventEmitter.SetParameter(this.parameterName, !remapRange ? this.value :
            this.value.Remap(0f, 1f, outputRange.from, outputRange.to));
    }
}
