﻿public static class FloatExtensions
{
    public class Range<T>
    {
        public T from;
        public T to;
    }

    [System.Serializable]
    public class FloatRange : Range<float> { }

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
